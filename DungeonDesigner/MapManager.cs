﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Threading.Tasks;
using DungeonDesigner.World;
using DungeonDesigner.Objects;

//MapManager (SINGLETON)
//Inherit from: iMessageListener
/*
 * Members: static MapManager instance
 *          List<Map> Maps - Container of different Map objects
 *          Map ActiveMap - The current Map
 *          MapTile currentView - The current MapTile
 *          
 * Accessors: Instance - Get the instance of MapManager
 *            mapActive - Get ActiveMap
 *           
 * Methods (see additional comments below):
 *          LoadMap - Loads in an XML format Map
 *          PlayerMove - Updates our view (text drawn)
 *          ProcessMessage - Handles received messages
 * 
 * Notes:   
 */

namespace DungeonDesigner
{
    class MapManager : iMessageListener
    {
        private static MapManager instance;
        private MapManager() { }

        public static MapManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new MapManager();
                return instance;
            }
        }

        private List<Map> Maps;
        private Map ActiveMap;
        public Map mapActive
        {
            set { ActiveMap = value; }
        }

        private MapTile currentView;

        public MapTile GetView()
        {
            return new MapTile(currentView);
        }

        public void Init()
        {
            Maps = new List<Map>();
            ActiveMap = new Map("Invalid", int.MaxValue, int.MaxValue);
            currentView = new MapTile();

            MessageManager.Instance.AddListener(this);
        }
        //TODO - Use strLoc to load the file

        //LoadMap
        /*
         * IN: string and bool
         * OUT: VOID
         * 
         * USE: Uses the IN string (strLoc) to locate and load in an XML file for a SINGLE MAP
         *      bActive sets the currentView to the loaded Maps starting tile
         */
        public void LoadMap(string strLoc, bool bActive = false)
        {
            Map temp;
            StreamReader sr = new StreamReader(strLoc);
            XmlReader reader = XmlReader.Create(sr);

            using (reader)
            {
                reader.ReadToFollowing("MAP");
                temp = new Map(reader.GetAttribute("name"), int.Parse(reader.GetAttribute("id")), int.Parse(reader.GetAttribute("start")));

                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                        if (reader.Name == "MAPTILE")
                        {
                            string strName;
                            int nId;
                            strName = reader.GetAttribute("name");
                            nId = int.Parse(reader.GetAttribute("id"));

                            reader.ReadToFollowing("DESC");
                            string strDesc = reader.ReadElementContentAsString();

                            reader.ReadToFollowing("MOBS");
                            List<BasicLife> mobs = new List<BasicLife>();
                            //todo, remove old constructors. this way of reading will use the same con everytime
                            reader.Read();
                            reader.Read();
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                //if (reader.ReadToDescendant("MOB") == false)
                                //    continue;
                                BasicLife mob = new BasicLife(reader.GetAttribute("name"));
                                mob.Health = int.Parse(reader.GetAttribute("hp"));
                                mob.Speed = int.Parse(reader.GetAttribute("spd"));
                                mob.Strength = int.Parse(reader.GetAttribute("str"));
                                mob.Defense = int.Parse(reader.GetAttribute("def"));

                                while (reader.NodeType != XmlNodeType.EndElement)
                                {
                                    reader.Read();
                                    if (reader.Name == "LIMB")
                                    {
                                        if (reader.GetAttribute("type") == "ARM")
                                            mob.AddArm(new BodyPart(reader.ReadElementContentAsString()));
                                        else if (reader.GetAttribute("type") == "LEG")
                                            mob.AddLeg(new BodyPart(reader.ReadElementContentAsString()));
                                    }
                                }
                                mobs.Add(mob);
                                MessageManager.Instance.SendMessage("ADDMOB", mob);
                                reader.Read();
                                reader.Read();
                            }

                            reader.ReadToFollowing("EXITS");
                            List<TileExit> exits = new List<TileExit>();
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if(reader.NodeType == XmlNodeType.Element)
                                {
                                    exits.Add(new TileExit(reader.Name, reader.ReadElementContentAsInt()));
                                }
                            }

                            temp.AddTile(new MapTile(nId, strName, strDesc, exits, mobs));
                        }
                }

                Maps.Add(temp);

                if (bActive == true)
                    foreach (Map m in Maps)
                        if (temp.ID == m.ID)
                        {
                            currentView = m.GetTile(temp.Start);
                            ActiveMap = m;
                        }
            }
        }

        //PlayerMove
        /*
         * IN: string
         * OUT: VOID
         * 
         * USE: Receives new MapTile ID based on player's movement choice
         *      If valid, sets the view to the new MapTile
         */
        private void PlayerMove(string dir)
        {
            //FINISH
            //Activate new room and deactivate old
            //This will only handle SAME MAP travel
            int nTileID = currentView.GetExit(dir);
            if (nTileID == -1)
            {
                MessageManager.Instance.SendMessage("ADDTEXT", "You cannot move that way!\n\n");
                return;
            }
            currentView = ActiveMap.GetTile(nTileID);
            MessageManager.Instance.SendMessage("DRAW");
        }

        //ProcessMessage
        /*
         * IN: Message (ref type) and object (ref type)
         * OUT: VOID
         * 
         * USE: Handles received messages
         */
        void iMessageListener.ProcessMessage(Message msg)
        {
            switch (msg.GetMsgType())
            {
                case "INIT":
                    Init();
                    break;
                case "DRAW":
                    currentView.Draw();
                    break;
                case "PMOVE":
                    PlayerMove(msg.GetData(0).ToString());
                    break;
                case "REMOVEMOB":
                    currentView.RemoveMob((BasicLife)msg.GetData(0));
                    break;
            }
        }
    }
}
