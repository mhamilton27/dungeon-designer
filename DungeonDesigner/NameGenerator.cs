﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner
{
    class NameGenerator : iMessageListener
    {
        private static NameGenerator instance;
        private NameGenerator() { }

        public static NameGenerator Instance
        {
            get
            {
                if (instance == null)
                    instance = new NameGenerator();
                return instance;
            }
        }

        void iMessageListener.ProcessMessage(Message msg)
        {

        }
    }
}
