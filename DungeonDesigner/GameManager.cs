﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Timers;

using DungeonDesigner.Objects;
using DungeonDesigner.World;

//GameManager (SINGLETON)
//Inherit from: iMessageListener
/*
 * Members: static GameManager instance
 *          Timer gTick - Our game processes a turn on gTick's elapsed event (game loop)
 *          
 *          
 * Accessors: Instance - Get the instance of GameManager
 * 
 * Method (see additional comments below): 
 *         ProcInput - Process player's input
 *         ProcessMessage - Handle received messages
 *         FindPlayerTarget - Translate attack/kill string to a specific BasicLife
 *            
 * Notes:   gTick elapses every 100 ms, then the game processes a step. This is the loop.
 */

namespace DungeonDesigner
{
    class GameManager : iMessageListener
    {
        private static GameManager instance;
        private GameManager() { }

        public static GameManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new GameManager();
                return instance;
            }
        }

        private Timer gTick;
        private void gTick_Elapsed(object source, ElapsedEventArgs e)
        {
            gTick.Enabled = false;
            MessageManager.Instance.SendMessage("PROCEED");
        }
        //TODO - MAKE/ADD Intro state
        public void Init()
        {
            MessageManager.Instance.AddListener(this);
            MapManager.Instance.Init();

            //FINISH
            gTick = new Timer();
            gTick.Elapsed += new ElapsedEventHandler(gTick_Elapsed);
            gTick.Interval = 100;
            gTick.Enabled = true;
        }

        //ProcInput
        /*
         * IN: string
         * OUT: VOID
         * 
         * USE: Checks user input and creates/sends out a message
         *      Will usually create an action for the player
         */
        public void ProcInput(string strCommand)
        {
            char[] splitter = {' '};
            string[] commands = strCommand.Split(splitter, 2);

            switch (commands[0])
            {
                case "e":
                case "E":
                    MessageManager.Instance.SendMessage("ADDACTION", 0, ActionTypes.MOV, "e");
                    break;
                case "n":
                case "N":
                    MessageManager.Instance.SendMessage("ADDACTION", 0, ActionTypes.MOV, "n");
                    break;
                case "s":
                case "S":
                    MessageManager.Instance.SendMessage("ADDACTION", 0, ActionTypes.MOV, "s");
                    break;
                case "w":
                case "W":
                    MessageManager.Instance.SendMessage("ADDACTION", 0, ActionTypes.MOV, "w");
                    break;
                case "attack":
                case "kill":
                    //Check for second word ("attack mob", not "attack")
                    if (commands.Length > 0)
                    {
                        int nBodyPart = 0;
                        BasicLife atkTarget = FindPlayerTarget(commands[1], out nBodyPart);

                        if (atkTarget == null)
                            MessageManager.Instance.SendMessage("ADDTEXT", "Could not find " + commands[1] + "!\n\n");
                        else
                            MessageManager.Instance.SendMessage("ADDACTION", 0, ActionTypes.ATK, atkTarget, nBodyPart);
                    }
                    break;
                case "l":
                case "look":
                    MessageManager.Instance.SendMessage("ADDACTION", 0, ActionTypes.LOK);
                    break;
                case "exa":
                case "ex":
                case "examine":
                    int nWaste = 0;
                    BasicLife exaTarget = FindPlayerTarget(commands[1], out nWaste);

                    MessageManager.Instance.SendMessage("ADDACTION", 0, ActionTypes.EXA, exaTarget);
                    break;
                case "":
                    break;
                default:
                    MessageManager.Instance.SendMessage("ADDTEXT", "Bad Command\n\n");
                    break;
            }
        }

        //FindPlayerTarget
        /*
         * IN: string and int(OUT)
         * OUT: BasicLife and int
         * 
         * USE: Takes in the user's target string (without "kill"/"attack") and finds the intended BasicLife target
         * Will also check the string if an arm or leg is being targeted
         */
        private BasicLife FindPlayerTarget(string atkTarget, out int nBodyPart)
        {
            //nContains: -1 = arm, 1 = leg, 0 = N/A
            nBodyPart = 0;

            //NOTE: It was decided that hard coding the fellowing cases is the safer/faster way
            if (atkTarget.Contains("leg "))
            {
                nBodyPart = 1;

                atkTarget = atkTarget.Remove(atkTarget.IndexOf("leg "), 4);
            }
            else if (atkTarget.Contains(" leg"))
            {
                nBodyPart = 1;

                atkTarget = atkTarget.Remove(atkTarget.IndexOf(" leg"), 4);
            }
            else if (atkTarget.Contains("arm "))
            {
                nBodyPart = -1;

                atkTarget = atkTarget.Remove(atkTarget.IndexOf("arm "), 4);
            }
            else if (atkTarget.Contains(" arm"))
            {
                nBodyPart = -1;

                atkTarget = atkTarget.Remove(atkTarget.IndexOf(" arm"), 4);
            }

            MapTile curView = MapManager.Instance.GetView();

            foreach (BasicLife potTarget in curView.GetMobs())
            {
                if (potTarget.Name.Contains(atkTarget))
                    return potTarget;
            }
            
            return null;
        }

        //ProcessMessage
        /*
         * IN: Message (ref type) and object(message sender)
         * OUT: VOID
         * 
         * USE: Receives message from MessageManager.
         *      Please see MessageManager and MessageListener
         */
        void iMessageListener.ProcessMessage(Message msg)
        {
            switch (msg.GetMsgType())
            {
                case "INPUT":
                    ProcInput(msg.GetData(0).ToString());
                    break;
                case "CONTINUE":
                    gTick.Enabled = true;
                    break;
            }
        }
    }
}
