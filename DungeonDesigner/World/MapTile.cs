﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonDesigner.Objects;

//MapTile
/*
 * Members: Name - Name of MapTile (DISPLAYED)
 *          Desc - Description of MapTile (DISPLAYED)
 *          int id - Unique ID# for the tile in this map
 *          List<BasicLife> lMobs - List of Mobs within this MapTile
 *          List<objet> lItems - List of Items within this MapTile
 *          List<TileExit> lExits - Possible room exits
 *          
 * Accessors: Name - Get/Set Name
 *            Desc - Get/Set Desc
 *            ID - Get id
 *            
 * Methods: ReqInit
 *          Draw - Sends message to Form for drawing view
 *          GetExit(string) - Returns a MapTile ID at the target exit
 * 
 * Notes:   
 */

namespace DungeonDesigner.World
{
    class MapTile
    {
        public string Name { get; set; }
        public string Desc { get; set; }

        private int id;
        private List<BasicLife> lMobs;
        private List<object> lItems;
        private List<TileExit> lExits;

        public int ID
        {
            get
            {
                return id;
            }
        }
        

        public MapTile()
        {
            Name = "Invalid";
            Desc = "Invalid Map Tile";
            id = int.MaxValue;
            lMobs = new List<BasicLife>();
            lItems = new List<object>();
            lExits = new List<TileExit>();
        }
        public MapTile(int ID, string name, string desc, List<TileExit> exits)
        {
            ReqInit(ID, name, desc, exits);
        }
        public MapTile(int ID, string name, string desc, List<TileExit> exits, List<BasicLife> mobs)
        {
            ReqInit(ID, name, desc, exits);

            foreach (BasicLife be in mobs)
                lMobs.Add(be);
        }
        public MapTile(MapTile toCopy)
        {
            Name = toCopy.Name;
            Desc = toCopy.Desc;
            id = toCopy.id;

            lMobs = new List<BasicLife>();
            for (int i = 0; i < toCopy.lMobs.Count; i++)
            {
                lMobs.Add(toCopy.lMobs[i]);
            }

            lItems = new List<object>();
            for (int i = 0; i < toCopy.lItems.Count; i++)
            {
                lItems.Add(toCopy.lItems[i]);
            }

            lExits = new List<TileExit>();
            for (int i = 0; i < toCopy.lExits.Count; i++)
            {
                lExits.Add(toCopy.lExits[i]);
            }
        }

        public BasicLife[] GetMobs()
        {
            return lMobs.ToArray();
        }

        public void RemoveMob(BasicLife mob)
        {
            lMobs.Remove(mob);
        }

        private void ReqInit(int ID, string name, string desc, List<TileExit> exits)
        {
            id = ID;
            Name = name;
            Desc = desc;

            lExits = new List<TileExit>();
            foreach (TileExit te in exits)
                lExits.Add(te);

            lMobs = new List<BasicLife>();
            lItems = new List<object>();
        }

        public void Draw()
        {
            MessageManager.Instance.SendMessage("ADDTEXT", Name + '\n');
            MessageManager.Instance.SendMessage("ADDTEXTCOLOR", Desc + '\n', System.Drawing.Color.Gray);

            string strExits = "Exits: ";
            foreach (TileExit te in lExits)
                strExits += (te.Name + ' ');
            MessageManager.Instance.SendMessage("ADDTEXTCOLOR", strExits + '\n', System.Drawing.Color.Green);
            
            //FINISH - Change drawing design
            foreach (BasicLife mob in lMobs)
            {
                MessageManager.Instance.SendMessage("ADDTEXTCOLOR", mob.Name + '\n', System.Drawing.Color.Yellow);
            }

            MessageManager.Instance.SendMessage("ADDTEXT", "\n");
        }

        public int GetExit(string dir)
        {
            foreach (TileExit te in lExits)
            {
                if (te.Name == dir)
                    return te.ID;
            }

            return -1; //MapManager knows if -1 is returned, the user's choice was not available
        }
    }

    struct TileExit
    {
        string strName;
        public string Name { get { return strName; } }
        int nID;
        public int ID { get { return nID; } }

        public TileExit(string name, int id)
        {
            strName = name;
            nID = id;
        }
    }
}
