﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Map
/*
 * Members: int id - Unique ID# of the Map
 *          int start - ID# of MapTile, the starting tile
 *          string name - name of the Map
 *          List<MapTile> lTiles - collection of every MapTile (the actual map)
 *          
 * Accessors: ID - Get id
 *            Start - Get start
 *            Name - Get name
 *            GetTile(int nID) - Returns MapTile with id == nID
 *            AddTile(MapTile mt) - Adds target MapTile to lTiles collection
 *            
 * Notes:   
 */

namespace DungeonDesigner.World
{
    class Map
    {
        private int id;
        public int ID { get { return id; } }
        private int start;
        public int Start { get { return start; } }
        private string name;
        public string Name { get { return name; } }
        private List<MapTile> lTiles;

        public Map(string strName, int nId, int nStart)
        {
            lTiles = new List<MapTile>();
            name = strName;
            start = nStart;
            id = nId;
        }

        public void AddTile(MapTile mt)
        {
            foreach (MapTile tile in lTiles)
            {
                if (tile.ID == mt.ID)
                    return;
            }

            lTiles.Add(mt);
        }

        public MapTile GetTile(int nID)
        {
            foreach (MapTile mt in lTiles)
            {
                if (mt.ID == nID)
                    return mt;
            }

            return new MapTile();
        }
    }
}
