﻿namespace DungeonDesigner
{
    partial class GameDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtb_View = new System.Windows.Forms.RichTextBox();
            this.tb_Input = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // rtb_View
            // 
            this.rtb_View.BackColor = System.Drawing.SystemColors.WindowText;
            this.rtb_View.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtb_View.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtb_View.ForeColor = System.Drawing.SystemColors.Window;
            this.rtb_View.Location = new System.Drawing.Point(0, 13);
            this.rtb_View.Name = "rtb_View";
            this.rtb_View.ReadOnly = true;
            this.rtb_View.Size = new System.Drawing.Size(709, 217);
            this.rtb_View.TabIndex = 0;
            this.rtb_View.Text = "";
            // 
            // tb_Input
            // 
            this.tb_Input.BackColor = System.Drawing.SystemColors.WindowText;
            this.tb_Input.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Input.ForeColor = System.Drawing.SystemColors.Window;
            this.tb_Input.Location = new System.Drawing.Point(0, 237);
            this.tb_Input.Name = "tb_Input";
            this.tb_Input.Size = new System.Drawing.Size(709, 13);
            this.tb_Input.TabIndex = 1;
            this.tb_Input.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_Input_KeyPress);
            // 
            // GameDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowText;
            this.ClientSize = new System.Drawing.Size(708, 262);
            this.Controls.Add(this.tb_Input);
            this.Controls.Add(this.rtb_View);
            this.Name = "GameDisplay";
            this.Text = "Dungeon Viewer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb_View;
        private System.Windows.Forms.TextBox tb_Input;
    }
}

