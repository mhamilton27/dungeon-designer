﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//iMessageListener (INTERFACE)
/*      
 * Notes:   Objects NEED to inherit from this interface to use the message system
 *          Objects NEED to be added to the MessageManager (AddListener)
 *          Objects NEED to write their own ProcessMessage
 */
namespace DungeonDesigner
{
    interface iMessageListener
    {
        void ProcessMessage(Message msg);
    }

    //Message
    /*
     * Members: string strType - Type of message
     *          List<object> - lData - Additional data needed for processing
     *          
     * Accessors: GetMsgType - Returns strType
     *            GetData(int i) - Returns lData[i]
     *            
     * Notes:   Message data can be any length, depends on the message type
     */
    class Message
    {
        private string strType;
        private List<object> lData;

        public Message(string str, params object[] data)
        {
            strType = str;
            lData = new List<object>();
            for (int i = 0; i < data.Length; i++)
            {
                lData.Add(data[i]);
            }
        }

        public string GetMsgType()
        {
            return strType;
        }
        public object GetData(int i)
        {
            return lData[i];
        }
    }
}
