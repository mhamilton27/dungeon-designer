﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//MessageManager (SINGLETON)
/*
 * Members: static MessageManager instance
 * List<iMessageListener> lListeners - Container for objects that plan to receive messages
 *          
 * Accessors: Instance - Get the instance of MessageManager
 * 
 * Method (see additional comments below): 
 *         AddListener - Adds an iMessageListener to lListeners
 *         SendMessage - Sends a message to all listeners in lListeners or specific target (overrides)
 *            
 * Notes:   To use this, inhereit object from iMessageListener and use the below AddListener method
 *          Will need to write a ProcessMessage function for each object
 */

namespace DungeonDesigner
{
    class MessageManager
    {
        private static MessageManager instance;
        private MessageManager() { lListeners = new List<iMessageListener>(); }

        public static MessageManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new MessageManager();
                return instance;
            }
        }

        private List<iMessageListener> lListeners;

        public void AddListener(iMessageListener obj)
        {
            lListeners.Add(obj);
        }

        //SendMessage
        /*
         * IN: Message (ref type) and Object (ref type)
         * OUT: VOID
         * 
         * USE: Calls ProcessMessage FOR EACH item in the lListeners container
         */
        public void SendMessage(string strMsgType, params object[] data)
        {
            Message msg = new Message(strMsgType, data);

            foreach (iMessageListener lis in lListeners)
            {
                lis.ProcessMessage(msg);
            }
        }

        //SendMessage
        /*
         * IN: Message (ref type), Object (ref type), and Object (ref type)
         * OUT: VOID
         * 
         * USE: Looks for a specific iMessageListener in lListeners to call ProcessMessage
         */
        public void SendMessage(string strMsgType, iMessageListener targ, params object[] data)
        {
            Message msg = new Message(strMsgType, data);

            foreach (iMessageListener lis in lListeners)
            {
                if (lis == targ)
                {
                    lis.ProcessMessage(msg);
                }
            }
        }
    }
}
