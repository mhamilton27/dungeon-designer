﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Objects
{
    class Player : BasicLife
    {
        public Player() : base("Player")
        {
            nID = 0;
            nHealth = 10;
            nSpeed = 3;
            nStrength = 4;
            nDefense = 2;

            lArms.Add(new BodyPart("Left Arm"));
            lArms.Add(new BodyPart("Right Arm"));

            lLegs.Add(new BodyPart("Left Leg"));
            lLegs.Add(new BodyPart("Right Leg"));
        }
    }
}
