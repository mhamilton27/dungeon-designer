﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Objects
{
    //LifeAction
    /*
     * Members: int nID - Type of action, see enum ActionTypes in LifeManager
     *          TimeSpan iniTime - Time of Day when the action is first processed
     *          List<object> data - Container for any data needed for the action type
     *          
     * Accessors: GetID - return nID
     *            InitialTime - get/set iniTime
     *            GetData(int i) - returns data[i]
     *            
     * Notes:   iniTime is set the first time an action goes through LifeManager.Process()
     */
    class LifeAction
    {
        private int nID;
        private TimeSpan iniTime;
        private List<object> data;

        public LifeAction(int ID, params object[] Data)
        {
            nID = ID;
            iniTime = new TimeSpan(0);
            data = new List<object>();
            for (int i = 0; i < Data.Length; i++)
            {
                data.Add(Data[i]);
            }
        }

        public int GetID { get { return nID; } }

        public TimeSpan InitialTime
        {
            get
            {
                return iniTime;
            }
            set
            {
                iniTime = value;
            }
        }

        public object GetData(int i)
        {
            return data[i];
        }

        public void AddData(object obj)
        {
            data.Add(obj);
        }
    }

    //BasicLife
    /*
     * Members: string strName - Name of the mob
     *          int nHealth - Health of the mob
     *          int nID - Unique ID of the mob
     *          int nSpeed - Speed of the mob
     *          Queue<LifeAction> actions - Container for mob's actions
     *          List<BodyPart> lArms - Experimental containers for combat
     *          List<BodyPart> lLegs
     *          
     * Accessors: Name - Get/Set strName
     *            Health - Get/Set nHealth
     *            ID - Get/Set nID
     *            Speed - Get/Set nSpeed
     *            GetAction - Gets the 1st LifeAction in the actions queue
     *            
     * Methods: AddArm(BodyPart arm) - Adds arm to lArms
     *          AddLeg(BodyPart leg) - Adds leg to lLegs
     *          UseAction() - Dequeues the first item in actions
     *          AddAction(LifeAction act) - Adds act to the actions queue
     *          
     *            
     * Notes:   
     */
    class BasicLife
    {
        protected string strName;
        public string Name
        {
            get
            {
                return strName;
            }
            set
            {
                strName = value;
            }
        }

        protected int nHealth;
        public int Health
        {
            get
            {
                return nHealth;
            }
            set
            {
                nHealth = value;
            }
        }
        protected int nID;
        public int ID
        {
            get
            {
                return nID;
            }
            set
            {
                nID = value;
            }
        }
        protected int nSpeed;
        public int Speed
        {
            get
            {
                return nSpeed;
            }
            set
            {
                nSpeed = value;
            }
        }
        protected int nStrength;
        public int Strength
        {
            get
            {
                return nStrength;
            }
            set
            {
                nStrength = value;
            }
        }
        protected int nDefense;
        public int Defense
        {
            get
            {
                return nDefense;
            }
            set
            {
                nDefense = value;
            }
        }

        protected Queue<LifeAction> actions;

        public BasicLife(string name)
        {
            lArms = new List<BodyPart>();
            lLegs = new List<BodyPart>();

            strName = name;
            nHealth = 1;
            nID = -1;
            nSpeed = 3;

            actions = new Queue<LifeAction>();
        }

        //FINISH
        //Add status effect variables
        protected List<BodyPart> lArms;
        protected List<BodyPart> lLegs;

        public void AddArm(BodyPart arm)
        {
            lArms.Add(arm);
        }

        public BodyPart GetArm()
        {
            if (lArms.Count == 0)
                return new BodyPart("INVALID");

            BodyPart tarArm = lArms[0];
            foreach (BodyPart arm in lArms)
            {
                if (arm.nHealth > 0 && arm.nDef <= tarArm.nDef)
                    tarArm = arm;
            }

            if (tarArm.nHealth <= 0)
                return new BodyPart("INVALID");
            return tarArm;
        }

        public void AddLeg(BodyPart leg)
        {
            lLegs.Add(leg);
        }

        public BodyPart GetLeg()
        {
            if (lLegs.Count == 0)
                return new BodyPart("INVALID");

            BodyPart tarLeg = lLegs[0];
            foreach (BodyPart leg in lLegs)
            {
                if (leg.nHealth > 0 && leg.nDef <= tarLeg.nDef)
                    tarLeg = leg;
            }

            if (tarLeg.nHealth <= 0)
                return new BodyPart("INVALID");
            return tarLeg;
        }

        public LifeAction GetAction()
        {
            if (actions.Count != 0)
                return actions.Peek();
            else
                return new LifeAction(-1);
        }

        public void UseAction()
        {
            actions.Dequeue();
        }

        public void AddAction(int nActType, params object[] data)
        {
            LifeAction newAction = new LifeAction(nActType, data);

            actions.Enqueue(newAction);
        }
    }

    //LifeAction
    /*
     * Members: string strName - Name of the BodyPart
     *          int nEQID - Item Manager ID of equiped item in this body slot
     *          int nDef - Defense of this BodyPart
     */
    class BodyPart
    {
        public BodyPart(string name)
        {
            strName = name;
            nEQID = -1;
            nHealth = 2;
            nDef = 0;
        }

        public string strName;
        public int nEQID;
        public int nDef;
        public int nHealth;
    }
}
