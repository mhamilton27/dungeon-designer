﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DungeonDesigner.Objects;

//LifeManager (SINGLETON)
//Inherit from: iMessageListener
/*
 * Members: static LifeManager instance
 *          List<BasicLife> lActiveMobs - List of actve mobs
 *          Player player - the player's object
 *          BasicLife[] mobs - Dynamic arrary of all mobs
 *          int nMobsItr - Iterator for mobs container
 *          int nMobsMax - Size of mobs container
 *          
 * Accessors: Instance - Get the instance of LifeManager
 *            
 * Methods (see additional comments below):
 *          AddMob - Adds mob to mobs container
 *          RemoveMob - Removes mob from mobs container
 *          Process - Process action for every living game object
 *          ProcessInitialMessage - Initiates a mob's action
 *          ProcessAction - Performs a mob's action
 *          ProcessMessage - Handle received messages
 * 
 * Notes:   
 */

namespace DungeonDesigner
{
    enum ActionTypes { ATK = 0, MOV, DTH, LOK, EXA };

    class LifeManager : iMessageListener
    {
        private static LifeManager instance;
        private LifeManager() {}

        public static LifeManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new LifeManager();
                return instance;
            }
        }

        private List<BasicLife> lActiveMobs;
        private Player player;
        private BasicLife[] mobs;
        private int nMobsItr;
        private int nMobsMax;

        public void Init()
        {
            nMobsMax = 50;
            mobs = new BasicLife[nMobsMax];
            nMobsItr = 0;
            lActiveMobs = new List<BasicLife>();
            player = new Player();
            MessageManager.Instance.AddListener(this);
        }

        //AddMob
        /*
         * IN: BasicLife (ref type)
         * OUT: VOID
         * 
         * USE: Adds target BasicLife to the mobs container and assigns target an ID
         *      Uses nMobsItr to keep track of items
         *      Uses nMobsMax to keep track of array size
         *      Recreates a new array of doubled size if needed
         *      
         * NOTE: The player is ID# 0, the FIRST BasicLife is ID# 1
         */
        private void AddMob(BasicLife mob)
        {
            mobs[nMobsItr] = mob;
            nMobsItr++;
            if (nMobsItr >= nMobsMax)
            {
                BasicLife[] temp = new BasicLife[nMobsMax * 2];

                for (uint i = 0; i < nMobsItr; i++)
                {
                    temp[i] = mobs[i];
                }

                mobs = temp;
                nMobsMax *= 2;
            }

            mob.ID = nMobsItr;

            return;
        }

        //RemoveMob
        /*
         * IN: BasicLife (ref type)
         * OUT: VOID
         * 
         * USE: Removes target BasicLife from the mobs container
         *      Creates a new container, skipping the target BasicLife
         *      BasicLifes after target will receive new IDs
         */
        private void RemoveMob(BasicLife mob)
        {
            BasicLife[] temp = new BasicLife[nMobsMax];

            for (int i = 0; i < mob.ID-1; i++)
            {
                temp[i] = mobs[i];
            }

            for (int i = mob.ID; i < nMobsItr; i++)
            {
                temp[i - 1] = mobs[i];
                temp[i - 1].ID = i;
            }

            nMobsItr--;
            mobs = temp;

            return;
        }

        //Process
        /*
         * IN: VOID
         * OUT: VOID
         * 
         * USE: Initiates/performs LifeAction for every BasicLife
         *      If the LifeAction.iniTime is zero, this method has not initiated it yet
         *      Processes the player first (since they are not stored in the array)
         *      Please see BasicLife for LifeAction
         */
        private void Process()
        {
            LifeAction act = player.GetAction();

            //No action to get, return
            if (act.GetID != -1)
            {
                if (act.InitialTime.TotalMilliseconds == 0)
                {
                    act.InitialTime = DateTime.Now.TimeOfDay;
                    ProcessInitialMessage(player, act);
                }
                else
                    ProcessAction(player, act);
            }

            for (int i = 0; i < nMobsItr; i++)
            {
                LifeAction mobAct = mobs[i].GetAction();

                if (mobAct.GetID != -1)
                {
                    if (mobAct.InitialTime.TotalMilliseconds == 0)
                    {
                        mobAct.InitialTime = DateTime.Now.TimeOfDay;
                        ProcessInitialMessage(mobs[i], mobAct);
                    }
                    else
                        ProcessAction(mobs[i], mobAct);
                }
            }

            MessageManager.Instance.SendMessage("CONTINUE");
        }

        //ProcessInitialMessage
        /*
         * IN: BasicLife (ref type) and LifeAction(ref type)
         * OUT: VOID
         * 
         * USE: Sends a message to display text, notifying the user of some BasicLife starting an action
         */
        private void ProcessInitialMessage(BasicLife mob, LifeAction action)
        {
            switch (action.GetID)
            {
                case (int)ActionTypes.ATK:
                    if ((int)action.GetData(1) == 0)
                        MessageManager.Instance.SendMessage("ADDTEXT", "You begin to swing at " + ((BasicLife)action.GetData(0)).Name + "!\n");
                    else // 1 = Leg, -1 = Arm
                    {
                        BasicLife tarLife = ((BasicLife)action.GetData(0));
                        BodyPart tarPart;

                        if ((int)action.GetData(1) == 1)
                            tarPart = tarLife.GetLeg();
                        else
                            tarPart = tarLife.GetArm();

                        if (tarPart.strName == "INVALID")
                        {
                            string strPartType = "";
                            if ((int)action.GetData(1) == 1)
                                strPartType = "leg";
                            else
                                strPartType = "arm";

                            MessageManager.Instance.SendMessage("ADDTEXT", tarLife.Name + " does not have a functional " + strPartType + "!\n");
                            mob.UseAction();
                            break;
                        }

                        action.AddData(tarPart);
                        MessageManager.Instance.SendMessage("ADDTEXT", "You begin to swing at " + ((BasicLife)action.GetData(0)).Name + "'s " + tarPart.strName + "!\n");
                    }

                    break;
                case (int)ActionTypes.MOV:
                    switch ((string)action.GetData(0))
                    {
                        case "e":
                        case "E":
                            MessageManager.Instance.SendMessage("ADDTEXT", "Player starts to run East!\n");
                            break;
                        case "n":
                        case "N":
                            MessageManager.Instance.SendMessage("ADDTEXT", "Player starts to run North!\n");
                            break;
                        case "s":
                        case "S":
                            MessageManager.Instance.SendMessage("ADDTEXT", "Player starts to run South!\n");
                            break;
                        case "w":
                        case "W":
                            MessageManager.Instance.SendMessage("ADDTEXT", "Player starts to run West!\n");
                            break;
                        default:
                            MessageManager.Instance.SendMessage("ADDTEXT", "Player starts to run " + (string)action.GetData(0) + "!\n");
                            break;
                    }
                    break;
                case (int)ActionTypes.DTH:
                    MessageManager.Instance.SendMessage("ADDTEXT", ((BasicLife)action.GetData(0)).Name + " falls over, spurting blood!\n");
                    break;
                case (int)ActionTypes.EXA:

                    break;
                case (int)ActionTypes.LOK:
                    MessageManager.Instance.SendMessage("ADDTEXT", "You take a quick look around the area...\n");
                    MessageManager.Instance.SendMessage("DRAW");

                    mob.UseAction();
                    break;
                default:
                    break;
            }
        }

        //ProcessAction
        /*
         * IN: BasicLife (ref type) and LifeAction(ref type)
         * OUT: VOID
         * 
         * USE: Checks the current time vs the initial time of the LifeAction,
         *      if the time is greater than a set # of milliseconds, perform the action
         * NOTE: BasicLife.UseAction(), ONLY calls Queue.Dequeue()
         */
        private void ProcessAction(BasicLife mob, LifeAction act)
        {
            TimeSpan curTime = DateTime.Now.TimeOfDay;

            switch (act.GetID)
            {
                case (int)ActionTypes.ATK:
                    if (curTime.TotalMilliseconds - act.InitialTime.TotalMilliseconds > mob.Speed * 1000)
                    {
                        BasicLife targetLife = (BasicLife)act.GetData(0);
                        Random randNum = new Random();
                        int nMobAtk = randNum.Next(mob.Strength);

                        //Case for not aiming at leg/arm
                        if ((int)act.GetData(1) == 0)
                        {
                            int nTarDef = randNum.Next(targetLife.Defense);
                            //Deal damage to target
                            if (nMobAtk > nTarDef)
                            {
                                targetLife.Health -= nMobAtk - nTarDef;

                                MessageManager.Instance.SendMessage("ADDTEXT", mob.Name + " hits " + targetLife.Name 
                                    + " for " + (nMobAtk - nTarDef).ToString() + " damage!\n\n");

                                //Target died, add Death action to its queue
                                if (targetLife.Health <= 0)
                                    targetLife.AddAction((int)ActionTypes.DTH, mob);
                            }
                            else
                                MessageManager.Instance.SendMessage("ADDTEXT", mob.Name + " did not hurt " + targetLife.Name + "!\n\n");
                        }
                        else
                        {
                            BodyPart tarPart;
                            if ((int)act.GetData(1) == 1)
                                tarPart = targetLife.GetLeg();
                            else
                                tarPart = targetLife.GetArm();

                            int nTarDef = randNum.Next(tarPart.nDef);

                            if (nMobAtk > nTarDef)
                            {
                                tarPart.nHealth -= (nMobAtk - nTarDef);

                                MessageManager.Instance.SendMessage("ADDTEXT", mob.Name + " hits " + targetLife.Name + "'s "
                                    + tarPart.strName + " for " + (nMobAtk - nTarDef).ToString() + " damage!\n\n");

                                if (tarPart.nHealth <= 0)
                                {
                                    MessageManager.Instance.SendMessage("ADDTEXT", targetLife.Name + "'s " + tarPart.strName + " is too damaged to function!\n\n");
                                }
                            }
                            else
                                MessageManager.Instance.SendMessage("ADDTEXT", mob.Name + " did not damage " + targetLife.Name + "'s "
                                    + tarPart.strName + "!\n\n");
                        }

                        mob.UseAction();
                    }
                    break;
                case (int)ActionTypes.MOV:
                    if (mob.ID == player.ID)
                    {
                        //Handle Move event for the player
                        //In real life moving isn't instant from room to room, so we can delay the player
                        if (curTime.TotalMilliseconds - act.InitialTime.TotalMilliseconds > 1000)
                        {
                            MessageManager.Instance.SendMessage("PMOVE", MapManager.Instance, act.GetData(0));

                            mob.UseAction();
                        }
                    }
                    else
                    {

                    }
                    break;
                case (int)ActionTypes.DTH:
                    if (mob.ID == player.ID)
                    {

                    }
                    else
                    {
                        MessageManager.Instance.SendMessage("ADDTEXT", mob.Name + " gives their dying breath!\n]n");

                        RemoveMob(mob);
                        MessageManager.Instance.SendMessage("REMOVEMOB", mob);
                    }
                    break;
                default:
                    break;
            }
        }

        //ProcessMessage
        /*
         * IN: Message (ref type) and object(message sender)
         * OUT: VOID
         * 
         * USE: Receives message from MessageManager.
         *      Please see MessageManager and MessageListener
         */
        void iMessageListener.ProcessMessage(Message msg)
        {
            switch (msg.GetMsgType())
            {
                case "ADDMOB":
                    AddMob((BasicLife)msg.GetData(0));
                    break;
                case "PROCEED":
                    Process();
                    break;
                case "ADDACTION":
                    {
                        int nID = (int)msg.GetData(0);
                        if (nID == 0)
                        {
                            switch ((int)msg.GetData(1))
                            {
                                case (int)ActionTypes.ATK:
                                    player.AddAction((int)ActionTypes.ATK, msg.GetData(2), msg.GetData(3));
                                    break;
                                case (int)ActionTypes.MOV:
                                    player.AddAction((int)ActionTypes.MOV, msg.GetData(2));
                                    break;
                                case (int)ActionTypes.EXA:
                                    player.AddAction((int)ActionTypes.EXA, msg.GetData(2));
                                    break;
                                case (int)ActionTypes.LOK:
                                    player.AddAction((int)ActionTypes.LOK);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    break;
            }
        }
    }
}
