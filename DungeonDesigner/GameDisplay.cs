﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DungeonDesigner
{
    public partial class GameDisplay : Form, iMessageListener
    {
        delegate void AddTextCallback(string text);
        delegate void AddTextColorCallback(string text, Color color);
        delegate void AddTextBackColorCallback(string text, Color f, Color b);

        public GameDisplay()
        {
            InitializeComponent();  
        }

        void Form1_Load(object sender, EventArgs e)
        {
            MessageManager.Instance.AddListener(this);
            GameManager.Instance.Init();
            LifeManager.Instance.Init();
            MapManager.Instance.LoadMap("TestRealm.xml", true); //FINISH

            MessageManager.Instance.SendMessage("DRAW");
            tb_Input.Select();
        }

        private void AddText(string text)
        {
            if (rtb_View.InvokeRequired)
            {
                AddTextCallback d = new AddTextCallback(AddText);
                this.Invoke(d, new Object[] { text });
            }
            else
            {
                rtb_View.SelectionStart = rtb_View.TextLength;
                rtb_View.SelectionLength = 0;
                rtb_View.AppendText(text);
                //rtb_View.SelectionStart = rtb_View.TextLength - text.Length; //Right align's text. ALL text on line
                //rtb_View.SelectionLength = text.Length;
                //rtb_View.SelectionAlignment = HorizontalAlignment.Right;
                rtb_View.Focus();
                tb_Input.Focus();
            }
        }
        private void AddText(string text, Color color)
        {
            if (rtb_View.InvokeRequired)
            {
                AddTextColorCallback d = new AddTextColorCallback(AddText);
                this.Invoke(d, new Object[] { text, color });
            }
            else
            {
                rtb_View.SelectionStart = rtb_View.TextLength;
                rtb_View.SelectionLength = 0;

                rtb_View.SelectionColor = color;
                rtb_View.AppendText(text);
                rtb_View.Focus();
                tb_Input.Focus();
            }
        }
        private void AddText(string text, Color front, Color back)
        {
            if (rtb_View.InvokeRequired)
            {
                AddTextBackColorCallback d = new AddTextBackColorCallback(AddText);
                this.Invoke(d, new object[] { text, front, back });
            }
            else
            {
                rtb_View.SelectionStart = rtb_View.TextLength;
                rtb_View.SelectionLength = 0;

                rtb_View.SelectionBackColor = back;
                rtb_View.SelectionColor = front;
                rtb_View.AppendText(text);
                rtb_View.Focus();
                tb_Input.Focus();
            }
        }

        void iMessageListener.ProcessMessage(Message msg)
        {
            switch (msg.GetMsgType())
            {
                case "ADDTEXT":
                    AddText((string)msg.GetData(0));
                    break;
                case "ADDTEXTCOLOR":
                    AddText((string)msg.GetData(0), (System.Drawing.Color)msg.GetData(1));
                    break;
                case "ADDTEXTCOLORBACK":
                    AddText((string)msg.GetData(0), (System.Drawing.Color)msg.GetData(1), (System.Drawing.Color)msg.GetData(2));
                    break;
            }
        }

        private void tb_Input_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                //Change MSG to Input
                MessageManager.Instance.SendMessage("INPUT", tb_Input.Text);
                tb_Input.Clear();
                e.Handled = true;
            }
        }
    }
}
