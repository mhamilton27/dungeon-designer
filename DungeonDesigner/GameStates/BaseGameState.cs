﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.GameStates
{
    abstract class BaseGameState
    {
        public abstract void Intro();
        public abstract void Proceed();
        public abstract void Exit();
        public abstract void ProcInput(string strCommand);
    }
}
