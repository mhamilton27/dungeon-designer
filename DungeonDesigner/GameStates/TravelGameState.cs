﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonDesigner;

namespace DungeonDesigner.GameStates
{
    class TravelGameState : BaseGameState
    {
        public override void Intro()
        {

        }

        public override void Proceed()
        {
            //TODO - Send Msg to LifeManager for finding active mobs
            //Send active mob + rand# between 0-speed to CombatManager
            //CombatManager stores all mobs in a list, in order of speed
            //actions process in that order

            //Change how "Active" works.
            //Make Active a Life member, remove active from maps
        }

        public override void Exit()
        {

        }

        public override void ProcInput(string strCommand)
        {
            switch (strCommand)
            {
                case "e":
                case "E":
                    MessageManager.Instance.SendMessage("PMOVE", "e"); 
                    break;
                case "n":
                case "N":
                    MessageManager.Instance.SendMessage("PMOVE", "n");
                    break;
                case "s":
                case "S":
                    MessageManager.Instance.SendMessage("PMOVE", "s");
                    break;
                case "w":
                case "W":
                    MessageManager.Instance.SendMessage("PMOVE", "w");
                    break;
                case "":
                    break;
                default:
                    MessageManager.Instance.SendMessage("ADDTEXT", "Bad Command\n\n");
                    break;
            }
        }
    }
}
