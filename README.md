Dungeon Designer

Allows users to create text-based(MUD-style) dungeons for their own custom adventures!
Right now this is a very, very early version with many features to come.

Current use:
The "TextRealm.xml" file is the current default map which the game loads.
Feel free to learn the current layout and make changes of your own, till the tool is created.
Note: The numbers in <EXITS></EXITS> are the IDs of the MapTile you will be entering.


Currently Implemented Commands:
'look' or 'l' : Look command, redraws the current view
'n','e','s','w' : Movement commands
'kill' or 'attack' : 1 round of player combat - DOES NOT AUTO ATTACK


We have implemented attacking limbs! With some auto-targeting (targets weakest arm/leg)
ex. Room has a mob named "Jimmy Clone"
'kill Clone' 'attack Jimmy' 'kill Jimmy Clone' - Basic attack
'kill leg Clone' 'attack Jimmy arm' - Targets the weakest leg/arm

Notice the limb can go before or after the mob's name!